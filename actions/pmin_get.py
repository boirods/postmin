import http.client

def parseQueries(queries):
    return '&'.join(queries)

def parseRecursos(path):
    return '?'.join(path)

def mainget(url, verbose=False, includeHeader=False):
    if len(url) > 3:
        try:
          url[3] = parseQueries(url[3])
          url[2] = parseRecursos([url[2],url[3]])
        except IndexError:
            pass
    url[1] = int(url[1])
    conn = None
    if verbose:
        conn = http.client.HTTPConnection(url[0], url[1])
        conn.debuglevel = 1
    else:
        conn = http.client.HTTPConnection(url[0], url[1])
    conn.request('GET', url[2])
    response = conn.getresponse()
    headers = dict(response.getheaders())
    headerCorreto = ''
    for k,v in headers.items():
      headerCorreto += f'{k}: {v}\n'
    body = response.read()
    if includeHeader:
        return (body, headerCorreto)
    else:
        return body.decode()

def get_response_and_headers(url, verbose=False):
    body, headers = mainget(url, verbose=verbose, includeHeader=True)
    return body, headers
