import sys
from actions.pmin_get import *
verbose = False
includeHeader = False
onlyHeader = False

#print(sys.argv)
def parseMyUrl(url):
    parseandoUrl = []
    parseandoUrl = url.split(':')

    #divide para pegar o recurso:
    recursosQuebrados = parseandoUrl[1].split('/')
    parseandoUrl[1] = recursosQuebrados[0]

    r = recursosQuebrados[1:]
    r = '/'.join(r)
    if '?' in url:
        r = r.split('?')

        queries = r[1]
        queries = queries.split('&')

        parseandoUrl.append('/'+r[0])
        parseandoUrl.append(queries)
    else:
        parseandoUrl.append(r)
    return parseandoUrl

if '-v' in sys.argv:
    verbose = True
if '-h' in sys.argv:
    includeHeader = True
if '-H' in sys.argv:
    onlyHeader = True

if sys.argv[1] == 'GET' or sys.argv[1] == 'get':
    myurl = sys.argv[2]
    myurl = parseMyUrl(myurl)
    if includeHeader and not verbose:
       b,h = get_response_and_headers(myurl, verbose)
       print(h)
       print(b)
    else:
        print(mainget(myurl, verbose, includeHeader))

if sys.argv[1] == 'POST' or sys.argv[1] == 'post':
    myurl = sys.argv[2]
    myurl = parseMyUrl(myurl)
